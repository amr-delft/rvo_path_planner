#ifndef _RVO_AGENT_TRAJ_H
#define _RVO_AGENT_TRAJ_H

#include "RVO.h"
#include <algorithm>
#include <iostream>
// #include <geometry_msgs/Vector3Stamped.h>
// #include <acl_msgs/Trajectory.h>
// #include <path_planner/Trajectory.h>

namespace RVO {
class AgentTraj{
public:
  AgentTraj(){};
  ~AgentTraj(){};

  size_t agent_id;

  std::vector<float> time_traj;
  std::vector<RVO::Vector2> pos_traj;
  std::vector<RVO::Vector2> vel_traj;
  std::vector<RVO::Vector2> pref_vel_traj;
  
  void set_agent_id(int id);
  void reset();
  int log_data(float sim_time, RVO::Vector2 position, RVO::Vector2 vel, RVO::Vector2 pref_vel);
  bool get_state_at_time(float t, Vector2& pos_out, Vector2& vel_out);

};

}

#endif /* _RVO_AGENT_TRAJ_H */