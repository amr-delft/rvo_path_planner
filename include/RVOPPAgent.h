#include <vector>
#include <deque>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/TwistStamped.h>
#include <raven_rviz/Waypoint.h>
#include <quad_control/Goal.h>
#include "AgentTraj.h"


class RVOPPAgent{
public:

  // Members
  int id;
  std::string name;
  geometry_msgs::PoseStamped pos_msg;
  geometry_msgs::TwistStamped vel_msg;

  ros::Time waypoints_issue_time;
  std::deque<geometry_msgs::Point> waypoint_queue;

  RVO::AgentTraj traj;

  float max_speed;
  float preview_time;
  float preview_time_acc;
  float acc_cap;
  float inAirThreshold;

  float land_height;

  float h_tolerance;
  float v_tolerance;
  float wall_bounce;

  bool reachedGoal_h;
  bool reachedGoal_v;
  bool isInAir;

  bool killMotorNow;

  // bool isAnchored;

  // Methods
  RVOPPAgent();
  ~RVOPPAgent();

  void setPos(geometry_msgs::PoseStamped pos){pos_msg = pos; updateStatus();}
  void setVel(geometry_msgs::TwistStamped vel){vel_msg = vel;}
  void setTraj(RVO::AgentTraj traj){this->traj = traj;}
  
  void updateStatus();
  // void updateAnchor();

  float getMaxSpeedForPlanner();
  RVO::Vector2 getPosVec2();
  RVO::Vector2 getVelVec2();
  RVO::Vector2 getWaypointVec2();

  void setWaypoints(std::vector<geometry_msgs::Point> waypoints, ros::Time issue_time);
  void setWaypoints(geometry_msgs::Point waypoint, ros::Time issue_time);

  bool hasReachedGoal();

  geometry_msgs::Point getCurrentWayPoint();
  quad_control::Goal getControlGoal();

};
