#!/usr/bin/env python
# license removed for brevity
import rospy
# Include the ROS msg to be send
from geometry_msgs.msg import Point
from geometry_msgs.msg import PoseStamped
from quad_control.msg import Goal

from rvo_path_planner.msg import CommandSequence
from rvo_path_planner.msg import CommandComplete

import numpy as np
import random
import time

# points = {  'a': Point(1.25,-2.75,1.0),
#             'b': Point(1.25,-5.25,1.0),
#             'c': Point(-1.25,-5.25,1.0),
#             'd': Point(-1.25,-2.75,1.0)}

# formation_list = [(['a','b','c','d'],5.0),
#     (['d','a','b','c'],5.0),
#     (['c','d','a','b'],5.0),
#     (['a','b','c','d'],5.0),
#     (['b','a','d','c'],5.0),
#     (['d','c','b','a'],5.0),
#     (['a','b','c','d'],5.0),]

class Commander(object):
    def __init__(self):
        current_time = rospy.Time.now()

        self.pub_command = rospy.Publisher('rvo_path_planner/command',CommandSequence, queue_size=10)
        self.sub_complete = rospy.Subscriber('rvo_path_planner/command_complete', CommandComplete, self.callback_complete)
        self.issue_time = {
            'BQ01s':current_time,
            'BQ02s':current_time,
            'BQ03s':current_time,
            'BQ04s':current_time}

    # def send_formation(self,formation):
    #     current_time = rospy.Time.now()
    #     for i, veh_name in enumerate(self.veh_names):
    #         command = CommandSequence()
    #         command.veh_name = veh_name
    #         command.waypoints.append(points[formation[i]])
    #         command.header.stamp = current_time
    #         self.pub_command.publish(command)
    #         rospy.sleep(0.015)
    #         # print command


    def get_random_point(self):
        return Point(np.random.uniform(-1.5,1.5,1)[0],np.random.uniform(-6,1.5,1)[0],1.0)

    def callback_complete(self, complete_msg):
        current_time = rospy.Time.now()
        complete_flag = complete_msg.success
        spent_time = current_time.to_sec() - complete_msg.issue_time.to_sec()
        print "%s spent %s sec" %(complete_msg.veh_name,spent_time)

        if complete_flag or spent_time > 10.0:
            command = CommandSequence()
            command.veh_name = complete_msg.veh_name
            command.waypoints.append(self.get_random_point())
            command.header.stamp = current_time
            self.pub_command.publish(command)

if __name__ == '__main__':
    rospy.init_node('commander',anonymous=False)
    commander = Commander()
    # rospy.sleep(2.5)
    # for formation in formation_list:
    #     commander.send_formation(formation[0])
    #     rospy.sleep(formation[1])
    rospy.spin()
