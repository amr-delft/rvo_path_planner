#include "RVOPPAgent.h"
RVOPPAgent::RVOPPAgent()
{
  reachedGoal_h = true;
  reachedGoal_v = true;
  h_tolerance = 0.075;
  v_tolerance = 0.02;
  isInAir = false;
  inAirThreshold = 0.3;
  land_height = 0.0;
  acc_cap = 0.5;

  killMotorNow = false;
  wall_bounce = 0.05;
  // isAnchored = true;
  
  // Populate the waypoint_queue with a single waypoint at the origin
  // Ideally this always immediately overwritted by it's actual position through the callback in rvo_path_node.
  geometry_msgs::Point point;
  waypoint_queue.push_back(point);

}
RVOPPAgent::~RVOPPAgent(){}

RVO::Vector2 RVOPPAgent::getPosVec2()
{
  return RVO::Vector2(pos_msg.pose.position.x,pos_msg.pose.position.y);
}

RVO::Vector2 RVOPPAgent::getVelVec2()
{
  return RVO::Vector2(vel_msg.twist.linear.x,vel_msg.twist.linear.y);
}

RVO::Vector2 RVOPPAgent::getWaypointVec2()
{
  // return RVO::Vector2(waypoint_queue.front().position.x,waypoint_queue.front().position.y);
  geometry_msgs::Point waypoint = getCurrentWayPoint();
  return RVO::Vector2(waypoint.x,waypoint.y);
}

float RVOPPAgent::getMaxSpeedForPlanner()
{
  if (not isInAir){
    return 0.0f;
  }
  else{
    return max_speed;
  }
}

bool RVOPPAgent::hasReachedGoal()
{
  if (waypoint_queue.size() <= 1){
    return reachedGoal_h && reachedGoal_v;
  }
  else{
    return false;
  }
}

void RVOPPAgent::updateStatus()
{
  // Set reachedGoal_h and _v and isInAir flags according to current state.
  float dist_to_goal_h = RVO::abs(getPosVec2() - getWaypointVec2());
  float dist_to_goal_v = std::abs(pos_msg.pose.position.z - getCurrentWayPoint().z); 
  reachedGoal_v = dist_to_goal_v < v_tolerance;
  reachedGoal_h = dist_to_goal_h < h_tolerance;

  isInAir = pos_msg.pose.position.z > inAirThreshold;

  // Move to next waypoint if the current one is reached.
  if (reachedGoal_h && reachedGoal_v && waypoint_queue.size() > 1){
    waypoint_queue.pop_front();
    reachedGoal_h = false;
    reachedGoal_v = false;
  }
}

// void RVOPPAgent::updateAnchor(){
//   if (not isInAir){
//     isAnchored = true;
//   }
//   else{
//     if (reachedGoal_h){
//       isAnchored = not reachedGoal_v;
//     }
//     else{
//       isAnchored = false;
//     }
//   }
// }

geometry_msgs::Point RVOPPAgent::getCurrentWayPoint(){
  // TODO: make sure not empty.
  return waypoint_queue.front();
}

void RVOPPAgent::setWaypoints(std::vector<geometry_msgs::Point> waypoints_vec, ros::Time issue_time)
{
  waypoint_queue.clear();
  for (int i = 0; i < waypoints_vec.size(); i++){
    waypoint_queue.push_back(waypoints_vec[i]);
  }
  reachedGoal_h = false;
  reachedGoal_v = false;
  waypoints_issue_time = issue_time;
}

void RVOPPAgent::setWaypoints(geometry_msgs::Point waypoint, ros::Time issue_time)
{
  std::vector<geometry_msgs::Point> waypoints_vec;
  waypoints_vec.push_back(waypoint);
  setWaypoints(waypoints_vec,issue_time);
}

quad_control::Goal RVOPPAgent::getControlGoal()
{
  // updateAnchor();
  // ROS_INFO_STREAM(name << " reachedGoal_h: " << reachedGoal_v 
  //   << " reachedGoal_v: " << reachedGoal_v
  //   << " inAir: " << isInAir
  //   << " isAnchored: " << isAnchored);

  RVO::Vector2 current_pos = getPosVec2();
  RVO::Vector2 current_vel = getVelVec2();
  RVO::Vector2 current_waypoint = getWaypointVec2();

  // Get goal by interpolation
  RVO::Vector2 goal_pos;
  RVO::Vector2 goal_vel;
  traj.get_state_at_time(preview_time,goal_pos,goal_vel);
  
  // Set goal
  quad_control::Goal goal;

  if (goal_vel.x() == 0.0){
    float sign = 1.0;
    if (current_waypoint.x() >= current_pos.x()){
      sign = -1.0;
    }
    // std::cout << name << " x == 0.0, sign =" << sign << std::endl;
    goal.pos.x = current_pos.x() + sign*wall_bounce;
  }
  else{
    goal.pos.x = goal_pos.x();
  }
  if (goal_vel.y() == 0.0){
    float sign = 1.0;
    if (current_waypoint.y() >= current_pos.y()){
      sign = -1.0;
    }
    // std::cout << name << "y == 0.0, sign =" << sign << std::endl;
    goal.pos.y = current_pos.y() + sign*wall_bounce;
    }
  else{
    goal.pos.y = goal_pos.y();
  }

  float waypoint_z = getCurrentWayPoint().z;
  float current_z = pos_msg.pose.position.z;
  goal.pos.z = waypoint_z;


  goal.vel.x = goal_vel.x();
  goal.vel.y = goal_vel.y();
  goal.vel.z = 0.0;
  

  // Compute desire accleration
  RVO::Vector2 goal_acc;
  traj.get_state_at_time(preview_time_acc,goal_pos,goal_vel);
  goal_acc = goal_vel - current_vel;

  if (RVO::abs(goal_acc) > acc_cap){
    goal_acc = RVO::normalize(goal_acc)*acc_cap;
  }



  if (not std::isnan(goal_acc.x())){
    // Command 0 acc if goal_vel is zero (for each direciton)
    // This is to avoid the quad being able to "drift" out of bound with a series of small accleratio commands
    if (goal_vel.x() == 0.0){
      goal.accel.x = 0.0;
    }
    else{
      goal.accel.x = goal_acc.x();
    }

    if (goal_vel.y() == 0.0){
      goal.accel.y = 0.0;
    }
    else{
      goal.accel.y = goal_acc.y();
    }
    goal.accel.z = 0.0;
  }

  // if (isAnchored){
  //   // Do vertical movements only
  //   goal.pos.x = pos_msg.pose.position.x;
  //   goal.pos.y = pos_msg.pose.position.y;
  //   float waypoint_z = waypoint_queue.front().position.z;
  //   float current_z = pos_msg.pose.position.z;
  //   if (waypoint_z > current_z){
  //     goal.pos.z = std::min(waypoint_z,current_z + 0.05f);
  //   }
  //   else{
  //     goal.pos.z = std::max(waypoint_z,current_z -0.05f);
  //   }
  // }
  // else{
  //   // Do horizontal motions only
  //   goal.pos.x = goal_pos.x();
  //   goal.pos.y = goal_pos.y();
  //   goal.pos.z = pos_msg.pose.position.z;
  //   // goal.pos.z = height_at_issue_time;
  //   goal.vel.x = goal_vel.x();
  //   goal.vel.y = goal_vel.y();
  //   goal.vel.z = 0.0;
  //   // Compute desire accleration
  //   RVO::Vector2 goal_acc;
  //   traj.get_state_at_time(preview_time_acc,goal_pos,goal_vel);
  //   goal_acc = goal_vel - getVelVec2();
  //   if (not std::isnan(goal_acc.x())){
  //     goal.accel.x = goal_acc.x();
  //     goal.accel.y = goal_acc.y();
  //     goal.accel.z = 0.0;
  //   }
  // }

//TAKEOFF     1
//CUT_POWER   2
//RESET_XY_INT  3
//RESET_XYZ_INT 4
//ATTITUDE    5

  if (goal.pos.z < land_height + 0.05){
  // CUT_POWER when height is lower than a negative value.
    goal.waypointType = 2;
  }
  else{
    goal.waypointType = 1;
  }

  if (killMotorNow){
    goal.waypointType = 2;
  }

  return goal;
}