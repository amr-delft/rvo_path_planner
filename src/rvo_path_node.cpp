#include <ros/ros.h>
#include <vector>
#include "RVOPathPlanner.h"
#include "AgentTraj.h"
#include <path_planner/GenPath.h>
#include <path_planner/Trajectory.h>
#include <raven_rviz/VehicleList.h> //TODO Add dependency 
#include <quad_control/Goal.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Point.h>
#include <rvo_path_planner/CommandSequence.h>
#include <rvo_path_planner/CommandComplete.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <raven_rviz/Waypoint.h>
#include <std_msgs/ColorRGBA.h>
#include <std_srvs/Empty.h>

#include "RVOPPAgent.h"

#include <vector>
#include <math.h>

class RVOPPNode{
public:
  ros::NodeHandle nh;
  ros::NodeHandle nh_p;

  ros::Subscriber sub_veh_list;
  ros::Subscriber sub_command_seq;
  ros::Publisher pub_traj_marker_array;
  ros::Publisher pub_command_complete;

  std::vector<std::string> veh_name_list;

  std::vector<RVOPPAgent> veh_list;
  
  std::vector<ros::Subscriber> sub_pos_list;
  std::vector<ros::Subscriber> sub_vel_list;
  std::vector<ros::Subscriber> sub_waypoint_list;

  std::vector<ros::Publisher> pub_goal_list;

  std::vector<ros::ServiceServer> srv_list;

  std::vector<std_msgs::ColorRGBA> color_list;


  // === Flags === //
  bool isActivated;

  // === Parameters === //
  double neighborDist;
  int maxNeighbors;
  double timeHorizon;
  double timeHorizonObst;
  double radius;
  double maxSpeed;
  double goalThreshold;
  double simTimeStep;
  int maxSteps;
  double minRadius;
  int numIterate;
  double previewTime;
  double previewTimeAcc;
  double accCap;
  double wallBounce;

  double defaultTakoffHeight;
  double defaultLandHeight;
  double defaultLowHeight;

  double h_tolerance;
  double v_tolerance;

  int room_type;

  // === Room and obstacle === //
  std::vector< std::vector<RVO::Vector2> > obstacles;

  // === Methods === //

  RVOPPNode()
  {
    nh = ros::NodeHandle();
    nh_p = ros::NodeHandle("~");

    isActivated = false;

    readParameters();
    populate_color_list();
    genObstacles();

    sub_veh_list = nh.subscribe("current_vehicles",1, &RVOPPNode::callback_veh_list, this);
    sub_command_seq = nh_p.subscribe("command",10, &RVOPPNode::callback_command, this);
    pub_traj_marker_array = nh.advertise<visualization_msgs::MarkerArray>("/RAVEN_vehicles", 1);
    pub_command_complete = nh_p.advertise<rvo_path_planner::CommandComplete>("command_complete",1);

    srv_list.push_back(nh_p.advertiseService("activate", &RVOPPNode::callback_srv_activate ,this));
    srv_list.push_back(nh_p.advertiseService("deactivate", &RVOPPNode::callback_srv_deactivate ,this));
    srv_list.push_back(nh_p.advertiseService("land_all", &RVOPPNode::callback_srv_land_all ,this));
    srv_list.push_back(nh_p.advertiseService("takeoff_all", &RVOPPNode::callback_srv_takeoff_all ,this));
    srv_list.push_back(nh_p.advertiseService("kill_all", &RVOPPNode::callback_srv_kill_all ,this));
    srv_list.push_back(nh_p.advertiseService("use_current_as_land_height", &RVOPPNode::callback_srv_land_height ,this));

    ROS_INFO_STREAM("Call service /activate or /takeoff_all to activate the rvo_path_planner node.");

  }
  ~RVOPPNode(){}

  void populate_color_list()
  {
    std_msgs::ColorRGBA color;
    color.r = 1.0;
    color.g = 0.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 1.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 0.0;
    color.b = 1.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 1.0;
    color.g = 0.0;
    color.b = 1.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 1.0;
    color.g = 1.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 1.0;
    color.b = 1.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.5;
    color.g = 0.0;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 0.5;
    color.b = 0.0;
    color.a = 1.0;
    color_list.push_back(color);
    color.r = 0.0;
    color.g = 0.0;
    color.b = 0.5;
    color.a = 1.0;
    color_list.push_back(color);
  }

  void readParameters()
  {
    // Read from ROS parameters
    ros::param::getCached("~neighborDist",neighborDist);
    ros::param::getCached("~maxNeighbors",maxNeighbors);
    ros::param::getCached("~timeHorizon",timeHorizon);
    ros::param::getCached("~timeHorizonObst",timeHorizonObst);
    ros::param::getCached("~radius",radius);
    ros::param::getCached("~maxSpeed",maxSpeed);
    ros::param::getCached("~goalThreshold",goalThreshold);
    ros::param::getCached("~simTimeStep",simTimeStep);
    ros::param::getCached("~maxSteps",maxSteps);
    ros::param::getCached("~numIterate",numIterate);
    ros::param::getCached("~minRadius",minRadius);
    ros::param::getCached("~previewTime",previewTime);
    ros::param::getCached("~previewTimeAcc",previewTimeAcc);
    ros::param::getCached("~accCap",accCap);
    ros::param::getCached("~wallBounce",wallBounce);
    ros::param::getCached("~defaultTakoffHeight",defaultTakoffHeight);
    ros::param::getCached("~defaultLandHeight",defaultLandHeight);
    ros::param::getCached("~defaultLowHeight",defaultLowHeight);
    ros::param::getCached("~h_tolerance",h_tolerance);
    ros::param::getCached("~v_tolerance",v_tolerance);
    ros::param::getCached("~room_type",room_type);
  }


  void genObstacles()
  {
    if (room_type == 0){
      std::vector<RVO::Vector2> obstacle1, obstacle2;
            // ===== The pillar ===== //
      obstacle1.push_back(RVO::Vector2(-2.4f,-3.7f));
      obstacle1.push_back(RVO::Vector2(-3.9f,-3.7f));
      obstacle1.push_back(RVO::Vector2(-4.2f,-4.35f));
      obstacle1.push_back(RVO::Vector2(-3.9f,-5.0f));
      obstacle1.push_back(RVO::Vector2(-2.6f,-5.0f));
      obstacle1.push_back(RVO::Vector2(-2.2f,-4.35f));
      obstacles.push_back(obstacle1);
    }

    std::vector<RVO::Vector2> obstacle2;
    if (room_type == 0){
      // ===== Full L-shape space  ===== //
      obstacle2.push_back(RVO::Vector2(2.0f, 2.5f));
      obstacle2.push_back(RVO::Vector2(2.0f, -6.9f));
      obstacle2.push_back(RVO::Vector2(-7.5f, -6.9f));
      obstacle2.push_back(RVO::Vector2(-7.5f, -1.8f));
      obstacle2.push_back(RVO::Vector2(-2.0f, -1.8f));
      obstacle2.push_back(RVO::Vector2(-2.0f, 2.5f));
      obstacles.push_back(obstacle2);
    }
    else if (room_type == 1){
      // ===== Only the main space ===== // 
      obstacle2.push_back(RVO::Vector2(2.0f, 2.5f));
      obstacle2.push_back(RVO::Vector2(2.0f, -6.9f));
      obstacle2.push_back(RVO::Vector2(-2.0f, -6.9f));
      obstacle2.push_back(RVO::Vector2(-2.0f, 2.5f));
      obstacles.push_back(obstacle2);
    }
  }

  int addVehicle(std::string veh_name)
  {
    int veh_index = veh_name_list.size();
    veh_name_list.push_back(veh_name);

    RVOPPAgent veh;
    veh.name = veh_name;
    veh.id = veh_index;
    veh.max_speed = maxSpeed;
    veh.preview_time = previewTime;
    veh.preview_time_acc = previewTimeAcc;
    veh.acc_cap = accCap;
    veh.wall_bounce = wallBounce;
    veh.h_tolerance = h_tolerance;
    veh.v_tolerance = v_tolerance;

    veh_list.push_back(veh);

    sub_pos_list.push_back(nh.subscribe<geometry_msgs::PoseStamped>(veh_name+"/pose", 1, boost::bind(&RVOPPNode::callback_pos, this, _1,veh_index)));
    sub_vel_list.push_back(nh.subscribe<geometry_msgs::TwistStamped>(veh_name+"/vel", 1, boost::bind(&RVOPPNode::callback_vel, this, _1,veh_index)));
    sub_waypoint_list.push_back(nh.subscribe<raven_rviz::Waypoint>(veh_name+"/quad_waypoint", 1, boost::bind(&RVOPPNode::callback_waypoint, this, _1,veh_index)));
    pub_goal_list.push_back(nh.advertise<quad_control::Goal>(veh_name+"/goal", 1));

    return veh_index;
  }

  void removeVehilce(std::string veh_name)
  {
    // TODO: Implement remove vehicle.
  }

  visualization_msgs::MarkerArray getTrajMarkerArray()
  {
    visualization_msgs::MarkerArray markerArray;

    for (int i = 0; i < veh_list.size(); i++){
      // === Trajectory === //
      RVO::AgentTraj agentTraj = veh_list[i].traj;

      visualization_msgs::Marker traj_marker;
      traj_marker.header.frame_id = "/world";
      traj_marker.header.stamp = ros::Time::now();
      traj_marker.ns = "rvo_path";
      traj_marker.id = i;
      traj_marker.action = visualization_msgs::Marker::ADD;
      traj_marker.lifetime = ros::Duration(1.0);
      traj_marker.type = visualization_msgs::Marker::LINE_STRIP;
      traj_marker.pose.orientation.w = 1.0;
      traj_marker.color = color_list[i];
      traj_marker.scale.x = 0.045;
      traj_marker.scale.y = 0.03;
      traj_marker.scale.z = 0.03;
      // Fill in the traj points
      for (int j = 0; j < agentTraj.pos_traj.size();j++){
        geometry_msgs::Point p;
        p.x = (float) agentTraj.pos_traj[j].x();
        p.y = (float) agentTraj.pos_traj[j].y();
        p.z = (float) 0.0;
        traj_marker.points.push_back(p);
      }
      markerArray.markers.push_back(traj_marker);

      // === Goal === //
      visualization_msgs::Marker goal_marker;
      goal_marker.header.frame_id = "/world";
      goal_marker.header.stamp = ros::Time::now();
      goal_marker.ns = "rvo_goal";
      goal_marker.id = i;
      goal_marker.action = visualization_msgs::Marker::ADD;
      goal_marker.lifetime = ros::Duration(1.0);
      goal_marker.type = visualization_msgs::Marker::CYLINDER;
      goal_marker.pose.position.x = veh_list[i].getCurrentWayPoint().x;
      goal_marker.pose.position.y = veh_list[i].getCurrentWayPoint().y;
      goal_marker.pose.position.z = veh_list[i].getCurrentWayPoint().z/2.0;
      goal_marker.pose.orientation.w = 1.0;
      goal_marker.color = color_list[i];
      goal_marker.color.a = 0.25;
      goal_marker.scale.x = 0.1;
      goal_marker.scale.y = 0.1;
      goal_marker.scale.z = veh_list[i].getCurrentWayPoint().z;
      markerArray.markers.push_back(goal_marker);

      // === Pos === //
      visualization_msgs::Marker pos_marker;
      pos_marker.header.frame_id = "/world";
      pos_marker.header.stamp = ros::Time::now();
      pos_marker.ns = "rvo_pos";
      pos_marker.id = i;
      pos_marker.action = visualization_msgs::Marker::ADD;
      pos_marker.lifetime = ros::Duration(1.0);
      pos_marker.type = visualization_msgs::Marker::CYLINDER;
      pos_marker.pose.position.x = veh_list[i].pos_msg.pose.position.x;
      pos_marker.pose.position.y = veh_list[i].pos_msg.pose.position.y;
      // pos_marker.pose.position.z = 0.0;
      pos_marker.pose.position.z = veh_list[i].pos_msg.pose.position.z;
      pos_marker.pose.orientation.w = 1.0;
      pos_marker.color = color_list[i];
      pos_marker.color.a = 0.5;
      pos_marker.scale.x = radius*2;
      pos_marker.scale.y = radius*2;
      pos_marker.scale.z = 0.01;
      markerArray.markers.push_back(pos_marker);
    }

    // === Obstacle === //
    for (int i = 0; i < obstacles.size(); i++){
      visualization_msgs::Marker obs_marker;
      obs_marker.header.frame_id = "/world";
      obs_marker.header.stamp = ros::Time::now();
      obs_marker.ns = "rvo_obs";
      obs_marker.id = i;
      obs_marker.action = visualization_msgs::Marker::ADD;
      obs_marker.lifetime = ros::Duration(1.0);
      obs_marker.type = visualization_msgs::Marker::LINE_STRIP;
      obs_marker.pose.orientation.w = 1.0;
      std_msgs::ColorRGBA color;
      color.r = 1.0;
      color.g = 0.0;
      color.b = 0.0;
      color.a = 1.0;
      obs_marker.color = color; 

      obs_marker.scale.x = 0.045;
      obs_marker.scale.y = 0.03;
      obs_marker.scale.z = 0.03;
      // Fill in the traj points
      for (int j = 0; j < obstacles[i].size(); j++){
        geometry_msgs::Point p;
        p.x = (float) obstacles[i][j].x();
        p.y = (float) obstacles[i][j].y();
        p.z = (float) 0.0;
        obs_marker.points.push_back(p);
      }
      // Add first point again
      geometry_msgs::Point p;
      p.x = (float) obstacles[i][0].x();
      p.y = (float) obstacles[i][0].y();
      p.z = (float) 0.0;
      obs_marker.points.push_back(p);


      markerArray.markers.push_back(obs_marker);
    }

    return markerArray;
  }

  bool planPath(){
    ros::Time startTime = ros::Time::now();
    RVO::RVOPathPlanner planner;
    // readParameters();

    planner.setAgentParameters(neighborDist, maxNeighbors, timeHorizon, timeHorizonObst, radius, maxSpeed);
    planner.setSimParameters(goalThreshold, simTimeStep, maxSteps, minRadius);

    // === Set agent and goal === //
    for(int i = 0; i < veh_list.size(); i++){
      // NOTE: Providing zero current velocity makes the quad harder to be pushed when stationary.
      // planner.addAgent(getRVOVector2(veh_pos_list[i]), getRVOVector2(veh_waypoint_list[i], RVO::Vector2(0.0,0.0));
      float veh_max_speed = veh_list[i].getMaxSpeedForPlanner();

      if (veh_list[i].name.compare("HL01") == 0){
        veh_max_speed = 0.0f;
      }

      planner.addAgent(veh_list[i].getPosVec2(),
        veh_list[i].getWaypointVec2(),
        veh_list[i].getVelVec2(),
        veh_max_speed);
    }

    // === Add obstacles === //
    for (int i = 0; i < obstacles.size(); i++){
      planner.addObstacle(obstacles[i]);
    }

    // Generate paths using RVOPathPlanner
    bool reachGoalFlag = planner.genPath(numIterate);

    // === Save the planned trajectorys === //
    for (int i = 0; i < planner.agentTrajVec.size(); i++){
      // veh_traj_list[i] = planner.agentTrajVec[i];
      veh_list[i].setTraj(planner.agentTrajVec[i]);
    }
    // ROS_INFO_STREAM("Computation Time:" << ros::Time::now() - startTime);
    return reachGoalFlag;
  }

  void publish_veh_goals()
  {
    for (int i = 0; i < veh_list.size(); i++){
      quad_control::Goal veh_goal = veh_list[i].getControlGoal();
      pub_goal_list[i].publish(veh_goal);
    }
  }

  void publish_command_complete()
  {
    // TODO: Add timeout mechanism
    for (int i = 0; i < veh_list.size(); i++){
      if (veh_list[i].hasReachedGoal()){  
        rvo_path_planner::CommandComplete comcom_msg;
        comcom_msg.veh_name = veh_list[i].name;
        comcom_msg.issue_time = veh_list[i].waypoints_issue_time;
        comcom_msg.header.stamp = ros::Time::now();
        comcom_msg.success = true;
        pub_command_complete.publish(comcom_msg);
      }
    }
  }

  // === Callbacks === //
  void callback_pos(const geometry_msgs::PoseStamped::ConstPtr& veh_pose_msg, int veh_index)
  {
    veh_list[veh_index].setPos(*veh_pose_msg);
    if (not isActivated){
      // Set waypoint as current pose for safe activation
      veh_list[veh_index].setWaypoints(veh_pose_msg->pose.position, veh_pose_msg->header.stamp);
    }

  }
  void callback_vel(const geometry_msgs::TwistStamped::ConstPtr& veh_vel_msg, int veh_index)
  {
    veh_list[veh_index].setVel(*veh_vel_msg);
  }
  void callback_waypoint(const raven_rviz::Waypoint::ConstPtr& veh_waypoint, int veh_index)
  {
    if (not isActivated){
      ROS_INFO_STREAM("rvo_path_planner is not activated. No commands sent to quad.");
    }
    else{
      veh_list[veh_index].setWaypoints(veh_waypoint->goal_pose.position,veh_waypoint->header.stamp);
      ROS_INFO_STREAM("Set " << veh_list[veh_index].name << " waypoint to"
        << " x: " << veh_waypoint->goal_pose.position.x
        << " y: " << veh_waypoint->goal_pose.position.y
        << " z: " << veh_waypoint->goal_pose.position.z );
    }
  }
  void callback_veh_list(const raven_rviz::VehicleList& veh_list_msg)
  {
    for (int i = 0; i < veh_list_msg.vehicle_names.size(); i++){
      std::string veh_name = veh_list_msg.vehicle_names[i];
      // TODO: check the BQ header.

      if (std::find(veh_name_list.begin(), veh_name_list.end(), veh_name) == veh_name_list.end()){
        // name is not in the veh_name_list, add it.
        int veh_index = addVehicle(veh_name);
        ROS_INFO_STREAM(veh_name << " added. Index: " << veh_index);
      }
    }
  }
  void callback_control(const ros::TimerEvent& timerEvent)
  {
    planPath();
    if (isActivated){
      // Only publish to /BQ##/goal when activated
      publish_veh_goals();
    }
  }

  void callback_command_complete(const ros::TimerEvent& timerEvent)
  {
    publish_command_complete();
  }
  
  void callback_markers(const ros::TimerEvent& timerEvent)
  {
    pub_traj_marker_array.publish(getTrajMarkerArray());
  }

  void callback_command(const rvo_path_planner::CommandSequence& command_seq)
  {
    // Find the index corresponding to the name
    // ROS_INFO_STREAM("Goal callback started.");

    // ROS_INFO_STREAM("Size: " << command_seq.veh_names.size());
    if (not isActivated){
      ROS_INFO_STREAM("rvo_path_planner is not activated. No commands sent to quad.");
    }
    else{
      std::string veh_name = command_seq.veh_name;
      std::vector<std::string>::iterator it = std::find(veh_name_list.begin(),veh_name_list.end(),veh_name);
      if (it == veh_name_list.end()){
        ROS_INFO_STREAM(veh_name << " not found in rvo_path_node.");
      }
      else{
        int index = it - veh_name_list.begin();
        if (command_seq.command==rvo_path_planner::CommandSequence::MOVE){
          veh_list[index].h_tolerance = h_tolerance;
          veh_list[index].v_tolerance = v_tolerance;
          veh_list[index].setWaypoints(command_seq.waypoints,command_seq.header.stamp);
          ROS_INFO_STREAM("Command " << command_seq.veh_name << " with " << command_seq.waypoints.size()<< " waypoints.");
        }
        else if (command_seq.command==rvo_path_planner::CommandSequence::TAKEOFF_AND_MOVE){
          veh_list[index].h_tolerance = h_tolerance;
          veh_list[index].v_tolerance = v_tolerance;
          std::vector<geometry_msgs::Point> waypoints_vec = getAccentWaypoints(veh_list[index].pos_msg, defaultTakoffHeight);
          waypoints_vec.insert(waypoints_vec.end(),command_seq.waypoints.begin(),command_seq.waypoints.end());
          veh_list[index].setWaypoints(waypoints_vec,command_seq.header.stamp);
          ROS_INFO_STREAM("Command " << command_seq.veh_name << " with " << command_seq.waypoints.size()<< " waypoints.");
        }
        else if(command_seq.command==rvo_path_planner::CommandSequence::LAND){
          veh_list[index].h_tolerance = 0.25;
          veh_list[index].v_tolerance = v_tolerance;
          // veh_list[index].setWaypoints(getDecentWaypoints(veh_list[index].pos_msg, defaultLandHeight),command_seq.header.stamp);
          veh_list[index].setWaypoints(getDecentWaypoints(veh_list[index].pos_msg, veh_list[index].land_height),command_seq.header.stamp);
          ROS_INFO_STREAM("Command " << command_seq.veh_name << " to " << "LAND");
        }
        else if(command_seq.command==rvo_path_planner::CommandSequence::TAKEOFF){
          veh_list[index].h_tolerance = h_tolerance;
          veh_list[index].v_tolerance = v_tolerance;
          ROS_INFO_STREAM("Command " << command_seq.veh_name << " to " << "TAKEOFF");
          veh_list[index].setWaypoints(getAccentWaypoints(veh_list[index].pos_msg, defaultTakoffHeight),command_seq.header.stamp);
        }
        else if(command_seq.command==rvo_path_planner::CommandSequence::LOW){
          veh_list[index].h_tolerance = 0.25;
          veh_list[index].v_tolerance = v_tolerance;
          ROS_INFO_STREAM("Command " << command_seq.veh_name << " to " << "LOW");
          veh_list[index].setWaypoints(getDecentWaypoints(veh_list[index].pos_msg, defaultLowHeight),command_seq.header.stamp);
          // Fix me?
        }
        else if(command_seq.command==rvo_path_planner::CommandSequence::KILL){
          veh_list[index].h_tolerance = h_tolerance;
          veh_list[index].v_tolerance = v_tolerance;
          ROS_INFO_STREAM("Command " << command_seq.veh_name << " to " << "KILL");
          veh_list[index].killMotorNow = true;
          // veh_list[index].setWaypoints(getAccentWaypoints(veh_list[index].pos_msg, defaultLowHeight),command_seq.header.stamp);
        }


      }
    }

  }

  std::vector<geometry_msgs::Point> getDecentWaypoints(geometry_msgs::PoseStamped currentPose, float goal_height)
  {
    // Generate landing waypoints given current pose
    std::vector<geometry_msgs::Point> waypoints;
    float z = currentPose.pose.position.z;
    while(z > goal_height){
      geometry_msgs::Point point;
      point.x = currentPose.pose.position.x;
      point.y = currentPose.pose.position.y;
      point.z = std::max(z,(float) goal_height);
      waypoints.push_back(point);
      z = z - 0.1;
    }
    return waypoints;
  }

  std::vector<geometry_msgs::Point> getAccentWaypoints(geometry_msgs::PoseStamped currentPose, float goal_height)
  {
    // Generate takeoff waypoints given current pose
    std::vector<geometry_msgs::Point> waypoints;
    // float z = currentPose.pose.position.z;
    // while(z < goal_height){
    geometry_msgs::Point point;
    point.x = currentPose.pose.position.x;
    point.y = currentPose.pose.position.y;
    // point.z = std::min(z,(float) goal_height);
    point.z = goal_height;
    waypoints.push_back(point);
    // z = z + 0.5;
    // }
    return waypoints;
  }

  void set_land_height(int veh_index){
    veh_list[veh_index].land_height = veh_list[veh_index].pos_msg.pose.position.z;
    ROS_INFO_STREAM(veh_list[veh_index].name << "land height set as " << veh_list[veh_index].land_height);
  }

  bool callback_srv_activate(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    isActivated = true;
    ROS_INFO_STREAM("rvo_path_node activated.");
    return true;
  }

  bool callback_srv_deactivate(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    isActivated = false;
    ROS_INFO_STREAM("rvo_path_node deactivated.");
    return true;
  }

  bool callback_srv_land_all(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    // Land all the quads
    ROS_INFO_STREAM("Landing all quads.");
    for(int i = 0; i < veh_list.size();i++){
      rvo_path_planner::CommandSequence command_seq;
      command_seq.veh_name = veh_list[i].name;
      command_seq.command = rvo_path_planner::CommandSequence::LAND;
      callback_command(command_seq);
    }
    return true;
  }

  bool callback_srv_takeoff_all(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {

    // Activate and take off all quads to the default height
    isActivated = true;
    ROS_INFO_STREAM("rvo_path_node activated.");
    for(int i = 0; i < veh_list.size();i++){
      // Set current height as land height.
      set_land_height(i);

      rvo_path_planner::CommandSequence command_seq;
      command_seq.veh_name = veh_list[i].name;
      command_seq.command = rvo_path_planner::CommandSequence::TAKEOFF;
      callback_command(command_seq);
    }
    return true;
  }  

  bool callback_srv_kill_all(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    // Kill all motors right away and deactivate
    for (int i = 0; i < veh_list.size();i++){
      // === Set current pos as waypoints === //
      veh_list[i].setWaypoints(veh_list[i].pos_msg.pose.position, ros::Time::now());
      // === Get the goal commands and overwrites to kill motor
      quad_control::Goal veh_goal = veh_list[i].getControlGoal();
      veh_goal.waypointType = 2;
      pub_goal_list[i].publish(veh_goal);
    }
    ROS_INFO_STREAM("Sent out CUT_POWER command.");
    isActivated = false;
    ROS_INFO_STREAM("rvo_path_planner deactivated.");

    return true;
  }

  bool callback_srv_land_height(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
  {
    // Kill all motors right away and deactivate
    for (int i = 0; i < veh_list.size();i++){
      set_land_height(i);
    }
    return true;
  }

};

void setDefaultParameters()
{
  // Set default parameters
  if (!ros::param::has("~neighborDist")) { ros::param::set("~neighborDist",15.0);}
  if (!ros::param::has("~maxNeighbors")) { ros::param::set("~maxNeighbors",15);}
  if (!ros::param::has("~timeHorizon")) { ros::param::set("~timeHorizon",1.5);}
  if (!ros::param::has("~timeHorizonObst")) { ros::param::set("~timeHorizonObst",0.25);}
  if (!ros::param::has("~radius")) { ros::param::set("~radius",0.35);}
  if (!ros::param::has("~maxSpeed")) { ros::param::set("~maxSpeed",1.8);}
  if (!ros::param::has("~goalThreshold")) { ros::param::set("~goalThreshold",0.1);}
  if (!ros::param::has("~simTimeStep")) { ros::param::set("~simTimeStep",0.01);}
  if (!ros::param::has("~maxSteps")) { ros::param::set("~maxSteps",300);}
  if (!ros::param::has("~numIterate")) { ros::param::set("~numIterate",0);}
  if (!ros::param::has("~minRadius")) { ros::param::set("~minRadius",0.1);}
  if (!ros::param::has("~previewTime")) { ros::param::set("~previewTime",0.3);}
  if (!ros::param::has("~previewTimeAcc")) { ros::param::set("~previewTimeAcc",0.01);}
  if (!ros::param::has("~accCap")) { ros::param::set("~accCap",0.5);}
  if (!ros::param::has("~wallBounce")) { ros::param::set("~wallBounce",0.05);}
  if (!ros::param::has("~defaultTakoffHeight")) { ros::param::set("~defaultTakoffHeight",1.0);}
  if (!ros::param::has("~defaultLandHeight")) { ros::param::set("~defaultLandHeight",0.0);}
  if (!ros::param::has("~defaultLowHeight")) { ros::param::set("~defaultLowHeight",0.5);}
  if (!ros::param::has("~h_tolerance")) { ros::param::set("~h_tolerance",0.075);}
  if (!ros::param::has("~v_tolerance")) { ros::param::set("~v_tolerance",0.02);}
  if (!ros::param::has("~room_type")) { ros::param::set("~room_type",0);}
}

int main(int argc, char **argv)
{
  ros::init (argc, argv, "rvo_path_planner");
  RVOPPNode path_node;
  setDefaultParameters();
  ros::Timer timer_contorl = path_node.nh.createTimer(ros::Duration(0.01), &RVOPPNode::callback_control, &path_node);
  ros::Timer timer_command_complete = path_node.nh.createTimer(ros::Duration(0.1), &RVOPPNode::callback_command_complete, &path_node);
  ros::Timer timer_markers = path_node.nh.createTimer(ros::Duration(1.0/60.0), &RVOPPNode::callback_markers, &path_node);
  ros::spin();

  // TODO: Figure out how asyncspinner work.
  // ros::AsyncSpinner spinner(4);
  // spinner.start();
  // ros::waitForShutdown();
  return 0;
}